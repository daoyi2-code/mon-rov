import { resolve } from 'path'
import { defineConfig, externalizeDepsPlugin } from 'electron-vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  main: {
    plugins: [externalizeDepsPlugin()]
  },
  preload: {
    plugins: [externalizeDepsPlugin()]
  },
  renderer: {
    resolve: {
      alias: {
        '@renderer': resolve('src/renderer/src'),
        '@':resolve('src/renderer')
      }
    },
    plugins: [vue()]
    // server:{
    //   proxy:{
    //   '/api':{
    //       target:'http://rqd-ThinkPad-T450:8088',
    //       changeOrigin:true,
    //       rewrite:(path)=>path.replace(/^\/api/,'')
    //     }
    //   }
    // }
  }
})
