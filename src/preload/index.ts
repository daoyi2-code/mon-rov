import { contextBridge } from 'electron'
import { electronAPI } from '@electron-toolkit/preload'
import { Buffer } from 'node:buffer';
import fs from 'node:fs'
// Custom APIs for renderer
const rqapi = {
    mk_str(){
        return "rqd-test-func!"
    },
    Buffer,
    readFile:fs.readFile,
    readFileSync:fs.readFileSync
}

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
    try {
        contextBridge.exposeInMainWorld('electron', electronAPI)
        contextBridge.exposeInMainWorld('rrapi', rqapi)
    } catch (error) {
        console.error(error)
    }
} else {
    // @ts-ignore (define in dts)
    window.electron = electronAPI
    // @ts-ignore (define in dts)
    window.api = rqapi
}
