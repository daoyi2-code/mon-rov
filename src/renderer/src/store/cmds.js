import { defineStore } from 'pinia'
import {ref,computed,reactive} from 'vue'

export const useCmdStore = defineStore('rr-cmds',()=>{
    const all_cmds=reactive([])
    
    function $reset(){
        all_cmds.length = 0;
    }
    
    function push(a){
        return all_cmds.push(a)
    }
    return {all_cmds,$reset,push}
})
    
    
    
    