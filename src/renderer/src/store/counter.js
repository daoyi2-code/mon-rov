import { defineStore } from 'pinia'
import {ref,computed} from 'vue'

export const useCounterStore = defineStore('rr-test-cnter',()=>{
    const count = ref(0)
    const double = computed(()=>{
        return count.value * 2
    })
    
    function inc(){
        count.value += 1
    }
    
    function $reset(){
        count.value = 0
    }
    return {count,double,inc,$reset}
})
