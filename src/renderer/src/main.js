import { createApp } from 'vue'
import './style.css'
//import '/src/assets/css/XP.css'
import App from './App.vue'
import router from "./router"

import {createPinia} from 'pinia'     // vuex 存储

createApp(App).use(router).use(createPinia()).mount('#app')
//createApp(App).use(createPinia()).mount('#app')
