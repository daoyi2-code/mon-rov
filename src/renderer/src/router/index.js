import {createRouter, createWebHashHistory } from "vue-router"

const routes=[
    {
        path:"/",
        name:"Home",
        component:()=>import("../components/HelloWorld.vue")
    },
    {
        path:"/test1",
        name:"TeKey",
        component:()=>import("../views/GotKey.vue")
    },
    {
        path:"/about2/:msg",
        name:"About2",
        props: true,
        component:()=>import("../views/Home.vue")
    },
    {
        path:"/outer",
        name:"Outer",
        // props: true,
        component:()=>import("../views/Outer.vue")
    }
    
]

const router=createRouter({
    history: createWebHashHistory(),
    routes
})

export default router;
